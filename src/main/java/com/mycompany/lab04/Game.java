/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab04;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {
    
    private Player player1;
    private Player player2;
    private Table table;
    public Game(){
        this.player1 = new Player('O');
        this.player2 = new Player('X');
        
    }
    
    public void newGame(){
        this.table = new Table(player1, player2);
    }
    
    public void play(){
        showWelcome();
        showTable();
        showTurn();
        inputRowCol();
        if(table.checkWin()){
            
        }
        if(table.checkDraw()){
            
        }
        table.switchPlayer();
    }
    
    public void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    
    public void showTable(){
        char[][] t = table.getTable();
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(t[i][j]+" ");
            }System.out.println();
        }
    }
    
    public void showTurn(){
        System.out.println(table.getCurrentPlayer().getSymbol()+" Turn");
    }
    
    public void inputRowCol(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Please input Row and Col: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row, col);
        
    }
    
}
